package com.example.s1512322.songle;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SettingsActivity extends AppCompatActivity {

    public int difficulty = 1;
    public boolean background = false;
    public List<SongParser.Song> songs = new ArrayList<>();
    public String string_songs;
    public String string_links;
    public final String TAG="Settings Activity";

    // Settings Activity is the activity in which the player selects its difficulty. They can also
    // select if they want background mode on or off. The difficulty is stored in shared preference
    // once the game starts.
    // In order for the Achievements Activity to work, we need to download the list of songs now.
    // We will get all the titles and all the Youtube links and pass them to the Achievements
    // Activity.

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Download the songs list.
        new DownloadWebpageSongs().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/songs.xml");

        // Allow 1 second for the download.
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // once songs are downloaded, get the links and titles.
        downloaded_songs();

        // Code to make image number X show up for difficulty X.
        Button startGame = (Button) findViewById(R.id.startButton);
        final ImageView image1 = (ImageView) findViewById(R.id.imageView5);
        final ImageView image2 = (ImageView) findViewById(R.id.imageView2);
        final ImageView image3 = (ImageView) findViewById(R.id.imageView3);
        final ImageView image4 = (ImageView) findViewById(R.id.imageView);
        final ImageView image5 = (ImageView) findViewById(R.id.imageView4);
        RadioButton rb1 = (RadioButton) findViewById(R.id.diff1);
        RadioButton rb2 = (RadioButton) findViewById(R.id.diff2);
        RadioButton rb3 = (RadioButton) findViewById(R.id.diff3);
        RadioButton rb4 = (RadioButton) findViewById(R.id.diff4);
        RadioButton rb5 = (RadioButton) findViewById(R.id.diff5);
        CheckBox cb = (CheckBox) findViewById(R.id.bakgroundMode);
        Button achievements = (Button) findViewById(R.id.achievementButton);

        achievements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToAchievements();
            }
        });

        final TextView explanation_box = (TextView) findViewById(R.id.textView3);

        rb1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeImagesInvisible();
                image1.setVisibility(View.VISIBLE);
                difficulty = 5;
                explanation_box.setText("Easiest mode: \n \n All the words are on the map. \n There are 4 categories to indicate which words are more important for guessing. ");
            }
        });

        rb2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeImagesInvisible();
                image2.setVisibility(View.VISIBLE);
                difficulty = 4;
                explanation_box.setText("Easier mode: \n \n All the words are on the map. \n There are 3 categories to indicate which words are more important for guessing. ");
            }
        });

        rb3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeImagesInvisible();
                image3.setVisibility(View.VISIBLE);
                difficulty = 3;
                explanation_box.setText("Medium: \n \n 75% of the words are on the map. \n There are 3 categories to indicate which words are more important for guessing. ");
            }
        });

        rb4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeImagesInvisible();
                image4.setVisibility(View.VISIBLE);
                difficulty = 2;
                explanation_box.setText("Hard: \n \n 50% the words are on the map. \n There are 2 categories to indicate which words are more important for guessing. ");
            }
        });

        rb5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeImagesInvisible();
                image5.setVisibility(View.VISIBLE);
                difficulty = 1;
                explanation_box.setText("Very hard: \n \n 25% the words are on the map. \n No other information is given. ");
            }
        });

        // Code to set background mode on/off.
        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                background = !background;
            }
        });

        makeImagesInvisible();

        /*
        When the startGame button is clicked, go to Maps activity and play with the selected difficulty.
        */
        startGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMap();
            }
        });
    }

    public void downloaded_songs() {
        Log.d(TAG,"downloaded_songs");
        // Get the songs and the links.
        setContentView(R.layout.activity_settings);
        string_songs = "";
        string_links = "";
        // Use ; as a separator for both links and strings.
        for (SongParser.Song song:songs){
            string_songs = string_songs + song.getTitle() + ";";
            string_links = string_links + song.getLink() + ";";
        }
    }

    // Function called to go to Maps Activity.
    private void goToMap(){
        Log.d(TAG,"goToMap");
        SharedPreferences sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Intent intent = new Intent(this, MapsActivity.class);
        editor.putString("difficulty",String.valueOf(difficulty));
        editor.commit();
        intent.putExtra("difficulty", String.valueOf(difficulty));
        intent.putExtra("background", background);
        startActivity(intent);
    }

    /*
     Function that makes all the images invisible. Used to display the right image when the user
     clicks on a difficulty.
    */

    private void makeImagesInvisible()
    {
        Log.d(TAG,"makeImagesInvisible");
        ImageView image1 = (ImageView) findViewById(R.id.imageView);
        ImageView image2 = (ImageView) findViewById(R.id.imageView2);
        ImageView image3 = (ImageView) findViewById(R.id.imageView3);
        ImageView image4 = (ImageView) findViewById(R.id.imageView4);
        ImageView image5 = (ImageView) findViewById(R.id.imageView5);
        image1.setVisibility(View.INVISIBLE);
        image2.setVisibility(View.INVISIBLE);
        image3.setVisibility(View.INVISIBLE);
        image4.setVisibility(View.INVISIBLE);
        image5.setVisibility(View.INVISIBLE);
    }

    // Function called to go to Achievements Activity.
    private void goToAchievements(){
        Log.d(TAG,"goToAchievements");
        Intent intent = new Intent(this, AchievementsActivity.class);
        intent.putExtra("songs", string_songs);
        intent.putExtra("links", string_links);
        startActivity(intent);
    }

    // This class and its methods are used for downloading the songs.

    private class DownloadWebpageSongs extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadSong(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
    }

    private String downloadSong(String myurl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d("DOWNLOAD", "The response is: " + response);
            is = conn.getInputStream();

            SongParser stackOverflowXmlParser = new SongParser();
            songs = stackOverflowXmlParser.parse(is);
            Log.d("DOWNLOAD", "NUM of songs is:" + songs.size());

            return "Done";

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } catch (XmlPullParserException e) {
            return "Xml Parser Exception";
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }
}
