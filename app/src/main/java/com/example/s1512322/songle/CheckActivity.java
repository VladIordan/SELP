package com.example.s1512322.songle;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class CheckActivity extends AppCompatActivity {

    String[][] lyrics = new String[300][300];
    int number_lines;
    File file;
    String artist="";
    String song="";
    public final String TAG="Check Activity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check2);
        Log.d(TAG, "onCreate");

        Bundle extras = getIntent().getExtras();
        artist = extras.getString("artist");
        song = extras.getString("song");

        // Get the word's coordinates and add them to the file.
        file = new File(getApplicationContext().getFilesDir(),"myfile.txt");
        String title = extras.getString("title");
        Position found_word = new Position(-1,-1);

        // Only add to file if the position is different from null.
        // This branch is not executed when the activity is reached via "View Words" button instead
        // of by finding a word.
        if(!title.equals("")) {
            addToFile(title);
            found_word = getPosition(title);
        }

        // Parse the lyrics
        String data = extras.getString("lyrics");
        Log.d(TAG, "The lyrics are: \n" + data);
        parseLyrics(data);


        // Read the file and add color to all the found positions.
        String raw_data = readFromFile(getApplicationContext());
        String[] positions = raw_data.split(";");
        if(!positions[0].equals("")){
            for( String pos:positions){
                Position position = getPosition(pos);

                // Color the found word with red and the others with blue.
                if(position.samePos(found_word)){
                    lyrics[position.getRow()][position.getCol()] = "<font color='#ff0000'>" +
                            lyrics[position.getRow()][position.getCol()] +
                            "</font>";
                }
                else{
                    lyrics[position.getRow()][position.getCol()] = "<font color='#3f51b5'>" +
                            lyrics[position.getRow()][position.getCol()] +
                            "</font>";
                }
            }
        }

        // Make the lyrics HTML format, so the colors are displayed on screen.

        // Concatenate all the word strings. The default color is the same as the background,
        // so if a word hasn't been colored at the previous step, it will not appear on screen.
        String toShow="<font color='#909090'> 1. </font>";
        int i = 0;
        int j = 0;
        while(i<number_lines){
            while(lyrics[i][j] != null) {
                toShow = toShow + lyrics[i][j] + " ";
                j++;
            }
            toShow = toShow + "\n <font color='#909090'> " + (i+2) +". </font>";
            i++;
            j=0;
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView text = (TextView) findViewById(R.id.text);
        try {
            text.setText(toShow);
            text.setTextColor(Color.WHITE);
            toShow = toShow.replaceAll("\r\n|[\r\n]", "<br>");
            text.setText(Html.fromHtml(toShow));
        } catch (Exception e) {
            // e.printStackTrace();
            text.setText("Error: can't show the lyrics.");
        }

        // Make the bulb icon go to Guess Activity.
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               goToGuessActivity();
            }
        });
    }

    private void goToGuessActivity()
    {
        Log.d(TAG, "goToGuessActivity");
        Intent intent = new Intent(this, GuessActivity.class);
        intent.putExtra("artist", artist);
        intent.putExtra("song", song);
        startActivity(intent);
    }

   // Function to return the text in a file
    public static String readFromFile(Context context) {

        Log.d("Check Activity", "readFromFile");
        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("myfile.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("Check Activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("Check Activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    // Function that turns a string into a 2D array of words.
    private void parseLyrics(String data){
        Log.d(TAG, "parseLyrics");
        String[] lines = data.split("\\n");
        int i = 0;
        for (String line:lines) {
            int j = 0;
            String[] words = line.split(" ");
            for(String w:words){
                lyrics[i][j] = w;
                j++;
            }
            i++;
        }
        number_lines = i;
    }

    // Function that given a string of form x;y returns a position of (x,y).
    private Position getPosition(String title){
        Log.d(TAG, "getPosition");
        String coordinates[] = title.split(":");
        int row = Integer.parseInt(coordinates[0]) - 1;
        int col = Integer.parseInt(coordinates[1]) - 1;
        Log.d(TAG, "row is " + (row-1));
        Log.d(TAG, "col is " + (col-1));
        return new Position(row,col);
    }

    // Function that adds a position to the file.
    // Used when a new word is found.
    public void addToFile(String title){
        Log.d(TAG, "addToFile");
        try(FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            Log.d(TAG, "Adding: "+title);
            out.println(title+";");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Class used to store a position of a word in a song.
    private class Position{
        private int row;
        private int col;

        Position(int row, int col){
            this.row = row;
            this.col = col;
        }

        public int getRow(){
            return this.row;
        }

        public int getCol(){
            return this.col;
        }

        public boolean samePos(Position a){
            return ((a.getCol() == this.getCol()) && (a.getRow() == this.getRow()));
        }
    }
}
