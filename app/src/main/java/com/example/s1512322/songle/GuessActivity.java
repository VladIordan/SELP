package com.example.s1512322.songle;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class GuessActivity extends AppCompatActivity {

    String artist_name = "";
    String song_name = "";
    public final String TAG="Guess Activity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_guess2);

        Bundle extras = getIntent().getExtras();
        artist_name = extras.getString("artist");
        song_name = extras.getString("song");

        Button artistButton = (Button) findViewById(R.id.artistButton);
        Button songButton =  (Button) findViewById(R.id.songButton);

        makeAllInvisible();

        artistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText artist = (EditText) findViewById(R.id.artistGuess);
                Log.d(TAG ,"User guessed artist as: " + artist_name);
                if(similarStrings(artist.getText().toString(),artist_name))
                {
                    ImageView image1 = (ImageView) findViewById(R.id.imageView6);
                    makeAllInvisible();
                    image1.setVisibility(View.VISIBLE);
                }
                else
                {
                    ImageView image2 = (ImageView) findViewById(R.id.imageView7);
                    makeAllInvisible();
                    image2.setVisibility(View.VISIBLE);
                }

            }
        });

        songButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText song = (EditText) findViewById(R.id.songGuess);
                Log.d(TAG,"User guessed song as " + song_name);
                if(!similarStrings(song.getText().toString(),song_name))
                {
                    ImageView image3 = (ImageView) findViewById(R.id.imageView9);
                    makeAllInvisible();
                    image3.setVisibility(View.VISIBLE);
                }
                else
                {
                    goToCongratulationsActivity();
                }
            }
        });
    }

    private void makeAllInvisible()
    {
        Log.d(TAG, "makeAllInvisible");
        ImageView image1 = (ImageView) findViewById(R.id.imageView6);
        ImageView image2 = (ImageView) findViewById(R.id.imageView7);
        ImageView image3 = (ImageView) findViewById(R.id.imageView9);
        image1.setVisibility(View.INVISIBLE);
        image2.setVisibility(View.INVISIBLE);
        image3.setVisibility(View.INVISIBLE);
    }

    private void goToCongratulationsActivity(){
        Log.d(TAG, "goToCongratulationsActivity");
        Intent intent = new Intent(this, CongratulationsActivity.class);
        startActivity(intent);
    }

    // check if two strings are similar.
    // Used to check if the written answer is correct.
    public static boolean similarStrings(String a, String b){

        // Remove punctuation
        a = clear_punctuation(a);
        b = clear_punctuation(b);

        // Split each string
        String as[] = a.split(" ");
        String bs[] = b.split(" ");
        if(as.length == bs.length)
        {
            int i = 0;
            int n = as.length;
            for(i=0; i<n; i++)
            {
                if(!sameString(as[i], bs[i])){
                    return false;
                }
            }
            return true;
        }
        return false;

    }

    public static String clear_punctuation(String a){
        a = a.replaceAll(","," ");
        a = a.replaceAll("!"," ");
        a = a.replaceAll(";"," ");
        a = a.replaceAll("."," ");
        a = a.replaceAll("\\?"," ");
        a = a.replaceAll(":"," ");
        return a;
    }

    public static boolean sameString(String a, String b){
        a = a.toLowerCase();
        b = b.toLowerCase();
        return a.equals(b);
    }
}
