package com.example.s1512322.songle;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class ShopActivity extends AppCompatActivity {

    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    public File file;
    public String data;
    public String[] dataPoints;
    Map<String, Integer> prices;
    public final String TAG = "Shop Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        prices = new HashMap<String, Integer>();
        prices.put("boring",2);
        prices.put("notboring",5);
        prices.put("interesting",10);
        prices.put("veryinteresting",25);

        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        String user = sharedPreferences.getString("user_email","");
        change_songle_number(0, user);

        Button endShop = (Button) findViewById(R.id.endShop);
        endShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Button buyBoring = (Button) findViewById(R.id.buyBoring);
        buyBoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy_word("boring");
            }
        });

        Button buyNotBoring = (Button) findViewById(R.id.buyNotBoring);
        buyNotBoring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy_word("notboring");
            }
        });

        Button buyInteresting = (Button) findViewById(R.id.buyInteresting);
        buyInteresting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy_word("interesting");
            }
        });

        Button buyVeryInteresting = (Button) findViewById(R.id.buyVeryInteresting);
        buyVeryInteresting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buy_word("veryinteresting");
            }
        });
    }

    // Function called after every purchase.
    // Used to add any bought word to the file.
    public void addToFile(String title){
        Log.d(TAG,"addToFile");
        try(FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            Log.d(TAG, "Adding: "+title);
            out.println(title+";");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Function that handles buying a word.
    // For the given type, it looks at the shared preference string of remaining values and selects
    // a random word from there.
    private void buy_word(String type){
        Log.d(TAG,"buy_word");
        Log.d(TAG,"Type is " + type);
        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        data = sharedPreferences.getString(type, "");
        String user = sharedPreferences.getString("user_email","");
        if(data == ""){
            // No more words of that type.
            Toast toast = Toast.makeText(this, "No more words of this type", Toast.LENGTH_SHORT);
            toast.show();
        } else if(prices.get(type) > getSongles(user)) {
            // Too few Songles to purchase.
            Toast toast = Toast.makeText(this, "Not enough songles", Toast.LENGTH_SHORT);
            toast.show();
        } else {
            // update Songle Amount.
            change_songle_number(0 - prices.get(type), user);

            // Choose a random word from the shared preferences string.
            editor.remove(type).apply();
            dataPoints = data.split(";");
            int len = dataPoints.length;
            Random generator = new Random();
            int chosen = generator.nextInt(len);

            // Remove the word from the shared preferences string.
            String newstring = "";
            for (int i = 0; i < len; i++) {
                if (i != chosen) {
                    newstring = newstring + dataPoints[i] + ";";
                }
            }
            editor.putString(type, newstring);
            editor.commit();

            // Add to file of found words the bought word.
            file = new File(getApplicationContext().getFilesDir(), "myfile.txt");
            String title = dataPoints[chosen];
            addToFile(title);
        }
    }

    // Update the box on the screen.
    private void update_box(int num){
        TextView account = findViewById(R.id.account);
        account.setText(num + "Songles");
    }

    // Change the songle number in shared preferences.
    private void change_songle_number(int amount, String user){
        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        String username = user + "_songles";
        int songles = sharedPreferences.getInt(username,0);
        songles = songles + amount;
        update_box(songles);
        editor.remove(username).apply();
        editor.putInt(username,songles);
        editor.commit();
    }

    // Get the number of Songles for a user.
    private int getSongles(String user){
        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        String username = user+"_songles";
        return sharedPreferences.getInt(username,0);
    }
}