package com.example.s1512322.songle;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;


public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener{

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION=1;
    private boolean mLocationPermissionGranted = false;
    private Location mLastLocation;
    private int REQUEST_LOCATION=0;
    // Distance till a word can be picked up.
    public final int DISTANCE =30;
    public final String TAG = "Maps Activity";


    List<MyXmlParser.Entry> entryList = new ArrayList<>();
    ArrayList<Marker> markerList = new ArrayList<>();
    List<SongParser.Song> songs = new ArrayList<>();
    String lyrics;
    int song_num;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;



    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        checkMarkers();
        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        change_songle_number(0, sharedPreferences.getString("user_email", ""));
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Log.d(TAG, "onCreate");
        downloadSongs();
        Log.d(TAG, "Chosen song is " + song_num);
        downloadLyricsForSong(song_num);
        downloadMapForShop(song_num);
        downloadMapForGame(song_num);

        if (!granted_permissions()) {
            request_permissions();
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if(mGoogleApiClient == null){
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

    }

    public void downloadSongs() {

        Log.d(TAG, "downloadSongs");
        // Download the songs and choose the number.
        new DownloadWebpageSongs().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/songs.xml");

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Random generator = new Random();
        song_num = generator.nextInt(songs.size());
        editor.putInt("chosen", song_num);
        editor.apply();
    }

    public void downloadLyricsForSong(int num) {
        Log.d(TAG, "downloadLyricsForSong");
        if (num < 9) {
            new DownloadLyricsTask().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/0" +
                    (num+1) + "/lyrics.txt");
        } else {
            new DownloadLyricsTask().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/" +
                    (num+1) + "/lyrics.txt");
        }
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void downloadMapForShop(int song_num)
    {
        Log.d(TAG, "downloadMapForShop");
        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (song_num < 9) {
            new DownloadWebpageTask().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/0" +
                    (song_num+1) + "/map5.kml");
            Log.d("DOWNLOAD", "Chosen song is " + song_num);
        } else {
            new DownloadWebpageTask().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/" +
                    (song_num+1) + "/map5.kml");
            Log.d("DOWNLOAD", "Chosen song is " + song_num);
        }

        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String boring = "";
        String notboring = "";
        String interesting = "";
        String veryinteresting = "";
        Log.d(TAG, "Number of words is " + entryList.size());
        for (int i = 0; i < entryList.size(); i++) {
            assert (entryList.get(i).getDescription() != "");
            assert (entryList.get(i).getName() != "");
            switch (entryList.get(i).getDescription()) {
                case "boring":
                    boring = boring + entryList.get(i).getName() + ";";
                    break;
                case "notboring":
                    notboring = notboring + entryList.get(i).getName() + ";";
                    break;
                case "veryinteresting":
                    veryinteresting = veryinteresting + entryList.get(i).getName() + ";";
                    break;
                case "interesting":
                    interesting = interesting + entryList.get(i).getName() + ";";
                    break;
                default:
                    break;
            }
        }
        editor.putString("boring", boring);
        editor.putString("notboring", notboring);
        editor.putString("interesting", interesting);
        editor.putString("veryinteresting", veryinteresting);
        editor.apply();
    }

    public void downloadMapForGame(int song_num){
        Log.d(TAG, "downloadMapForGame");
        String diff = null;
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            diff = extras.getString("difficulty");
        }
        if (song_num < 10) {
            new DownloadWebpageTask().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/0" +
                    (song_num+1) + "/map" + diff + ".kml");
        } else {
            new DownloadWebpageTask().execute("http://www.inf.ed.ac.uk/teaching/courses/selp/data/songs/" +
                    (song_num+1) + "/map" + diff + ".kml");
        }
        try {
            TimeUnit.MILLISECONDS.sleep(50);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (mGoogleApiClient.isConnected())
        {
            mGoogleApiClient.disconnect();
        }
    }


    /* This function gets called when the map is ready to be used.
       Will try creating the screen for the start of the game.
       If the permissions are not granted, it will request them again.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady");
        mMap = googleMap;
        BitmapDescriptor bmd;
        if (granted_permissions()) {
            try {
                for (int i = 0; i < entryList.size(); i++) {
                    assert (entryList.get(i).getDescription() != null);

                    // Decide which icon corresponds based on the description.
                    switch (entryList.get(i).getDescription()) {
                        case "boring":
                            bmd = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("boring", 112, 112));
                            break;
                        case "notboring":
                            bmd = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("notboring", 112, 112));
                            break;
                        case "veryinteresting":
                            bmd = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("veryinteresting", 112, 112));
                            break;
                        case "interesting":
                            bmd = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("interesting", 112, 112));
                            break;
                        default:
                            bmd = BitmapDescriptorFactory.fromBitmap(resizeMapIcons("unclassified", 112, 112));
                            break;
                    }

                    // Get the coordinates.
                    double latitude = Double.parseDouble(entryList.get(i).getLatitude());
                    double longitude = Double.parseDouble(entryList.get(i).getLongitude());
                    LatLng latLng = new LatLng(latitude, longitude);

                    // Add the marker with the appropriate parameters.
                    Marker m = mMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .icon(bmd)
                            .title(entryList.get(i).getName())
                            .snippet(entryList.get(i).getDescription()));
                    markerList.add(m);
                }
                load_map();
            } catch (SecurityException se) {
                System.out.println("Security exception thrown[onMapReady]");
            }
        } else {
            request_permissions();
        }
    }

    protected void createLocationRequest()
    {
        // Set the parameters for the location request
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); // preferably every 5 seconds
        mLocationRequest.setFastestInterval(1000); // at most every second
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        int permissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck == PackageManager.PERMISSION_GRANTED)
        {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient,
                    mLocationRequest,
                    (com.google.android.gms.location.LocationListener) this);
        }
    }

    //Get the last known location of a device.
    @Override
    public void onConnected(Bundle connectionHint)
    {
        try
        {
            createLocationRequest();
        }
        catch (java.lang.IllegalStateException ise)
        {
            System.out.println("IllegalStateException thrown [onConnected]");
        }

        // Can we access the user’s current location?
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED)
        {
            mLastLocation =
                    LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        }
        else
        {
            ActivityCompat.requestPermissions(this,
                    new String[]
                            {
                                    Manifest.permission.ACCESS_FINE_LOCATION
                            }
                    , PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    // If in background mode, when location changed, pick up all words in range and remove all their
    // markers.
    @Override
    public void onLocationChanged(Location current) {
        Log.d(TAG,"(" + String.valueOf(current.getLatitude()) + "," + String.valueOf(current.getLongitude()) + ")");
        boolean background = getIntent().getExtras().getBoolean("background");
        ArrayList<Integer> toRemove = new ArrayList<>();
        setZoom(current);
        if(background){
            int len = markerList.size();
            for (int i=0; i<len; i++){
                Marker marker = markerList.get(i);
                boolean isClose = closeEnough(marker.getPosition());
                if(isClose){
                    sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
                    change_songle_number(1, sharedPreferences.getString("user_email", ""));
                    String desc = marker.getSnippet();
                    String title = marker.getTitle();
                    updateSharedPreferencesStrings(title, desc);
                    addToFile(title);
                    toRemove.add(0,i);
                }
            }
            for (int i: toRemove){
                markerList.get(i).remove();
                markerList.remove(i);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int flag)
    {
        System.out.println(">>>>onConnectionSuspended");
    }
    @Override
    public void onConnectionFailed(ConnectionResult result)
    {
// An unresolvable error has occurred and a connection to Google APIs
// could not be established. Display an error message, or handle
// the failure silently
        System.out.println(">>>>onConnectionFailed");
    }



    public void setZoom(Location location) {
        Log.d(TAG,"setZoom");
        double currentLongitude = location.getLongitude();
        double currentLatitude = location.getLatitude();

        LatLng latLng = new LatLng(currentLatitude, currentLongitude);
        CameraPosition position = mMap.getCameraPosition();
        CameraPosition.Builder builder = new CameraPosition.Builder();
        builder.zoom(20);
        builder.target(latLng);

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(builder.build()));
    }

    public void load_map() throws SecurityException {
        try {
            Log.d(TAG, "load_map");
            sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
            change_songle_number(0, sharedPreferences.getString("user_email", ""));
            boolean background = getIntent().getExtras().getBoolean("background");
            Log.d("DOWNLOAD", "Function load_map is reached ");
            // Visualise current position with a small blue circle
            mMap.setMyLocationEnabled(true);

            // Add ‘‘My location’’ button to the user interface
            mMap.getUiSettings().setMyLocationButtonEnabled(true);


            // Assure that the file used to store the current state of the game is empty.
            File file = new File(getApplicationContext().getFilesDir(), "myfile.txt");
            PrintWriter writer = null;
            try {
                writer = new PrintWriter(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            writer.print("");
            writer.close();

            // Enable a pop-up on a marker clicked if it is close enough.
            if(!background){
                mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(final Marker marker) {
                        if(closeEnough(marker.getPosition())){
                            AlertDialog.Builder dialog = new AlertDialog.Builder(MapsActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
                            dialog.setMessage("Do you want to pick-up this word?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
                                            change_songle_number(1, sharedPreferences.getString("user_email", ""));
                                            String desc = marker.getSnippet();
                                            String title = marker.getTitle();
                                            updateSharedPreferencesStrings(title, desc);
                                            goToCheckActivity(title);
                                            markerList.remove(marker);
                                            marker.remove();
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                            AlertDialog alert = dialog.create();
                            alert.setTitle("A word was found!");
                            alert.show();
                        }
                        return true;
                    }
                });
            }

            // Enable button click listeners.
            Button shopButton = (Button) findViewById(R.id.shopButton);
            shopButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToShop();
                }
            });

            Button checkButton = (Button) findViewById(R.id.checkButton);
            checkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToCheckActivity("");
                }
            });

            Button guessButton = (Button) findViewById(R.id.guessButton);
            guessButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToGuessActivity();
                }
            });
        } catch (SecurityException se) {
            System.out.println("Security exception thrown[onMapReady]");
        }


    }

    // Function to start a new Activity.
    // This function is called when a word has been collected.
    public void goToCheckActivity(String title) {
        Log.d(TAG,"goToCheckActivity");
        Intent intent = new Intent(this, CheckActivity.class);
        intent.putExtra("title", title);
        intent.putExtra("lyrics", lyrics);
        intent.putExtra("artist", songs.get(song_num).getArtist());
        intent.putExtra("song", songs.get(song_num).getTitle());
        startActivity(intent);
    }

    private void goToGuessActivity() {
        Log.d(TAG, "goToGuessActivity");
        Intent intent = new Intent(this, GuessActivity.class);
        intent.putExtra("artist", songs.get(song_num).getArtist());
        intent.putExtra("song", songs.get(song_num).getTitle());
        startActivity(intent);
    }

    public void goToShop() {
        Log.d(TAG, "goToShop");
        Intent intent = new Intent(this, ShopActivity.class);
        startActivity(intent);
    }

    // Everytime a word is picked up, the strings which contain all the remaining words have to
    // be updated.
    private void updateSharedPreferencesStrings(String name, String desc) {
        Log.d(TAG, "updateSharedPreferencesStrings");
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch (desc) {
            case "boring":
                String boring = sharedPreferences.getString("boring", "");
                editor.remove("boring").apply();
                boring = update_string(boring, name);
                editor.putString("boring", boring);
                editor.apply();
                break;
            case "notboring":
                String notboring = sharedPreferences.getString("notboring", "");
                editor.remove("notboring").apply();
                notboring = update_string(notboring, name);
                editor.putString("notboring", notboring);
                editor.apply();
                break;
            case "veryinteresting":
                String veryinteresting = sharedPreferences.getString("veryinteresting", "");
                editor.remove("veryinteresting").apply();
                veryinteresting = update_string(veryinteresting, name);
                editor.putString("veryinteresting", veryinteresting);
                editor.apply();
                break;
            case "interesting":
                String interesting = sharedPreferences.getString("interesting", "");
                editor.remove("interesting").apply();
                interesting = update_string(interesting, name);
                editor.putString("interesting", interesting);
                editor.apply();
                break;
            default:
                break;
        }
    }

    private String update_string(String data, String position) {
        Log.d(TAG,"update_string");
        String separator = ";";
        return data.replaceAll(position + separator, "");
    }

    // Function to resize a Bitmap. Used for scaling the placemarks.
    public Bitmap resizeMapIcons(String iconName, int width, int height) {
        Log.d(TAG,"resizeMapIcons");
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(iconName, "drawable", getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }

    // Function to check that all permissions have been granted
    // Currently only checking Location
    private boolean granted_permissions() {
        return (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED);
    }

    // This function will be called when the user answers the location request.
    // If the answer is positive, the program will load the screen to start the game.
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
                    if (mGoogleApiClient == null) {
                        mGoogleApiClient = new GoogleApiClient.Builder(this)
                                .addConnectionCallbacks(this)
                                .addOnConnectionFailedListener(this)
                                .addApi(LocationServices.API)
                                .build();
                    }
                }
            } else {
                request_permissions();
            }
        }
        return;
    }

    // Function to request permissions from the user.
    public void request_permissions() {
        Log.d(TAG, "Permissions not granted");
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_LOCATION);
    }

    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
    }

    private String downloadUrl(String myurl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is: " + response);
            is = conn.getInputStream();

            MyXmlParser stackOverflowXmlParser = new MyXmlParser();
            entryList = stackOverflowXmlParser.parse(is);

            Log.d(TAG, "Number of markers is " + entryList.size());

            return "Done";

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } catch (XmlPullParserException e) {
            return "Xml Parser Exception";
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private class DownloadWebpageSongs extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadSong(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
    }

    private String downloadSong(String myurl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is: " + response);
            is = conn.getInputStream();

            SongParser stackOverflowXmlParser = new SongParser();
            songs = stackOverflowXmlParser.parse(is);
            Log.d(TAG, "NUM of songs is:" + songs.size());

            return "Done";

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } catch (XmlPullParserException e) {
            return "Xml Parser Exception";
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }


    private class DownloadLyricsTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadLyrics(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
    }

    private String downloadLyrics(String myurl) throws IOException {
        InputStream is = null;
        try {
            URL url = new URL(myurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
            Log.d(TAG, "The response is: " + response);
            is = conn.getInputStream();
            String text = convertStreamToString(is);
            lyrics = text;
            return text;
        } catch (Exception e) {
            return "Exception";
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    // HELPER FUNCTIONS

    // Assure that all markers on the map do not correspond to guessed words.
    private void checkMarkers() {
        Log.d(TAG,"checkMarkers");
        for (Marker m : markerList) {
            String title = m.getTitle();
            if (searchInFile(title)) {
                m.remove();
            }
        }
    }

    // Look through the guessed words and return whether a word is in the list or not.
    private boolean searchInFile(String title) {
        Log.d(TAG, "searchInFile");
        String raw_data = CheckActivity.readFromFile(getApplicationContext());
        return raw_data.contains(title);
    }

    // Update Songle Amount on screen
    private void update_box(int num) {
        Log.d(TAG, "update_box");
        TextView songleAmount = findViewById(R.id.songles);
        songleAmount.setText(num + "Songles");
    }

    // Update Songle Amount in Shared Preferences.
    private void change_songle_number(int amount, String user) {
        Log.d(TAG, "change_songle_number");
        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        String username = user + "_songles";
        int songles = sharedPreferences.getInt(username, 0);
        songles = songles + amount;
        update_box(songles);
        editor.remove(username).apply();
        editor.putInt(username, songles);
        editor.apply();
    }

    private Location getCurrentLocation() {
        Log.d(TAG,"getCurrentLocation");
        Location location = null;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        return location;
    }

    // Check if the user is close enough to a given location.
    private boolean closeEnough(LatLng latLng){
        Log.d(TAG,"closeEnough");
        Location location = getCurrentLocation();
        Location markerLocation = new Location("marker clicked");
        markerLocation.setLatitude(latLng.latitude);
        markerLocation.setLongitude(latLng.longitude);
        double dist = location.distanceTo(markerLocation);
        return (dist < DISTANCE);
    }

    // Function called when a word is guessed via Background Mode.
    public void addToFile(String title){
        Log.d(TAG,"addToFile");
        File file = new File(getApplicationContext().getFilesDir(), "myfile.txt");
        try(FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw))
        {
            out.println(title+";");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




