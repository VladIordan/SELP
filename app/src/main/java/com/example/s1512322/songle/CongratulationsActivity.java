package com.example.s1512322.songle;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.zxing.common.StringUtils;

public class CongratulationsActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public final String TAG = "CongratulationsActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate");
        setContentView(R.layout.activity_congratulations);
        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        TextView message = findViewById(R.id.message);
        String user = sharedPreferences.getString("user_name","Player");
        String msg = "Well done, " + user + "! The song you just guessed has been added " +
                "to your achievements. Thank you for playing Songle and feel free " +
                "to play again!";
        message.setText(msg);
        update_achievements();

        Button restartButton = findViewById(R.id.restartButton);
        restartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToSettings();
            }
        });


    }

    // Function called to update the user's achievements and store them in shared preferences.
    private void update_achievements(){
        Log.d(TAG,"update_achievements");
        sharedPreferences = getSharedPreferences("SongleSavedPreferences", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        String user = sharedPreferences.getString("user_email","");
        String achievements = sharedPreferences.getString(user+"_achievements","");
        editor.remove(user+"_achievements").apply();
        String difficulty = sharedPreferences.getString("difficulty","");
        int chosen = sharedPreferences.getInt("chosen", -1);
        String new_achievements = update_achievements_string(achievements, chosen, difficulty);
        editor.putString(user + "_achievements", new_achievements);
        editor.commit();
    }

    // Function called to update the string which represent the achievements.
    // The achievement string is a sequence of digits between 0-5.
    // The i-th letter represents the hardest difficulty the i-th song has been guessed or 0 if that
    // song was not guessed.
    private String update_achievements_string(String achievements, int chosen_song, String difficulty){
        Log.d(TAG,"update_achievements_string");
        String new_achievements="";
        int len = achievements.length();
        // If a song with a high index has been guessed,
        // copy the string, add zeros, add the difficulty of the guessed song.
        if (len <= chosen_song){
            new_achievements = "" + achievements;
            for (int i = len; i<chosen_song; i++){
                new_achievements = new_achievements + "0";
            }
            new_achievements = new_achievements + difficulty;
        }
        // Else copy all the string and check if the character corresponding to the song has to be
        // changed.
        else{
            new_achievements = new_achievements + achievements.substring(0,chosen_song);
            if((Integer.parseInt(difficulty) < Integer.parseInt("" + achievements.charAt(chosen_song)))
              ||(Integer.parseInt("" + achievements.charAt(chosen_song))==0)){
                new_achievements = new_achievements + difficulty;
            }
            else{
                new_achievements = new_achievements + "" + achievements.charAt(chosen_song);
            }
            new_achievements = new_achievements + achievements.substring(chosen_song+1,len);
        }
        return new_achievements;
    }

    private void goToSettings(){
        Log.d(TAG, "goToSettings");
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }
}
